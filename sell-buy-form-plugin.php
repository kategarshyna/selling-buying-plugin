<?php
/*
 * @package SellBuyFormPlugin
 *
 * Plugin Name: Sell Buy Form Plugin
 * Plugin URI: https://bitbucket.org/kategarshyna/selling-buying-plugin/src
 * Description: Plugin for adding a contact form to the site
 * Author: Kate Garshyna
 * Version: 1.0
 * Author URI: https://bitbucket.org/kategarshyna/
 * License: GPLv2 or later
 * Text Domain: wptestproject.amtago.ml
*/

use App\Init;


defined( 'ABSPATH' ) or die;

if ( file_exists(dirname( __FILE__ ) . '/vendor/autoload.php') ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

if ( class_exists('App\\Init') ) {
	Init::initialize();
}

