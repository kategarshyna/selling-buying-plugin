(function () {
    jQuery( document ).ready(function() {

        jQuery('#plugin_settings_contact_form input, #plugin_settings_contact_form textarea').on('blur', function() {
            saveSettings(jQuery(this).serialize() + '&field=' + jQuery(this).attr('name'));
        });

        jQuery('#plugin_settings_contact_form .submit').on('click', function() {
            jQuery(this).attr('disabled', true);
            jQuery('#loader-contact-form-plugin').show();
            saveSettings(jQuery('#plugin_settings_contact_form form').serialize());
        });

    });

    function saveSettings(data) {
        jQuery.post( ajax_object.ajax_url, data + '&action=save_contact_form_settings', function(response) {
            jQuery('#loader-contact-form-plugin').hide();
            jQuery('#plugin_settings_contact_form .submit').removeAttr('disabled');
            checkResponse(response, '#plugin_settings_contact_form');
        });
    }
    function checkResponse (response, block) {
        jQuery('.result-msg').removeClass('text-danger').removeClass('text-success').text(' ');

        if (response.status) {
            jQuery('#plugin_contact_form input, #plugin_contact_form textarea').removeClass('has-error').removeClass('has-success').val('');
            jQuery('.result-msg').text(response.messages).addClass('text-success');
        } else {
            for (var key in response.messages) {
                var item = [];
                if (item = jQuery(block + ' [name="' + key + '"]') ) {
                    item.removeClass('has-error').removeClass('has-success');
                    if ( response.messages[key] == 'success') {
                        item.addClass('has-success');
                        item.parent().find('.error-msg').text('');
                    } else {
                        item.addClass('has-error');
                        item.parent().find('.error-msg').text(response.messages[key]);
                    }
                }
            }
        }
    }
})();
