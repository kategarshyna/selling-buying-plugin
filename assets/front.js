(function () {

    jQuery( document ).ready(function() {
        jQuery('#plugin_contact_form input, #plugin_contact_form textarea').on('blur', function() {
            sendPost(jQuery(this).serialize() + '&field=' + jQuery(this).attr('name'), false);
        });

        jQuery('#plugin_contact_form .submit').on('click', function() {
            jQuery(this).attr('disabled', true);
            jQuery(this).find('.loader-contact-form-plugin').show();
            sendPost(jQuery('#plugin_contact_form form').serialize(), true);
        });

        jQuery('#plugin_contact_form .step-1 .outline-button').on('click', function() {
            jQuery('#plugin_contact_form .step-1 .outline-button').removeClass('selected');
            jQuery('#plugin_contact_form .step-1 h1').removeClass('selected');
            jQuery(this).addClass('selected');
            if (jQuery(this).hasClass('selling')) {
                jQuery('#plugin_contact_form h1.selling').addClass('selected');
                jQuery('#plugin_contact_form h1.buying').removeClass('selected');
                jQuery('#plugin_contact_form .step-1 input[name=type]').val('Selling');
            }else {
                jQuery('#plugin_contact_form h1.buying').addClass('selected');
                jQuery('#plugin_contact_form h1.selling').removeClass('selected');
                jQuery('#plugin_contact_form .step-1 input[name=type]').val('Buying');
            }
        });

        jQuery('#plugin_contact_form .step-2 .submit').on('click', function() {
            jQuery('input[name=kind]').val(jQuery(this).find('p').text());
        });
        jQuery('#plugin_contact_form .step-3 .submit').on('click', function() {
            jQuery('input[name=spend]').val(jQuery(this).text());
        });
        jQuery('#plugin_contact_form .step-4 .submit').on('click', function() {
            jQuery('input[name=timeline]').val(jQuery(this).text());
        });
        jQuery('#plugin_contact_form .step-5 .submit').on('click', function() {
            jQuery('input[name=property]').val(jQuery(this).text());
        });

    });

    function sendPost(data, submit) {
        jQuery.post( ajax_object.ajax_url, data + '&action=front_contact_form_action', function(response) {
            jQuery('.loader-contact-form-plugin').hide();
            jQuery('#plugin_contact_form .submit').removeAttr('disabled');
            checkResponse(response, '#plugin_contact_form');
            if (submit && jQuery('#plugin_contact_form .active .error-msg').text().length <= 0) {
                jQuery('#plugin_contact_form .active').addClass('hidden').removeClass('active').next().removeClass('hidden').addClass('active');
            }
        });
    }
    function checkResponse (response, block) {
        jQuery('.result-msg').removeClass('text-danger').removeClass('text-success').text(' ');

        if (response.status) {
            jQuery('#plugin_contact_form input, #plugin_contact_form textarea').removeClass('has-error').removeClass('has-success').val('');
            jQuery('.result-msg').text(response.messages).addClass('text-success');
        } else {
            for (var key in response.messages) {
                var item = [];
                if (item = jQuery(block + ' [name="' + key + '"]') ) {
                    item.removeClass('has-error').removeClass('has-success');
                    if ( response.messages[key] == 'success') {
                        item.addClass('has-success');
                        item.parent().find('.error-msg').text('');
                    } else {
                        item.addClass('has-error');
                        item.parent().find('.error-msg').text(response.messages[key]);
                    }
                }
            }
        }
    }
})();
