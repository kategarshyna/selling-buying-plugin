<?php
/**
 * Trigger this file on Plugin uninstall
 *
 * @package AlecadddPlugin
 */

defined( 'WP_UNINSTALL_PLUGIN' ) or die;

if ( file_exists(dirname( __FILE__ ) . '/vendor/autoload.php') ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

$contacts = get_posts( array( 'post_type' => \App\Models\ContactFormModel::post_type, 'numberposts' => -1 ) );

foreach( $contacts as $contact ) {
	wp_delete_post( $contact->ID, true );
}

delete_option (\App\Base\Config::$plugin_option);
