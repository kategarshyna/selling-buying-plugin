<div id="plugin_contact_form">
    <form>
        <div class="step-1 active">
            <h1 class="selling <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?> "><?php echo $settings['selling_step_1'] ?></h1>
            <h1 class="buying <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>" ><?php echo $settings['buying_step_1'] ?></h1>
            <div class="button-group">
                <a class="selling btn outline-button <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?>">Seller</a>
                <a class="buying btn outline-button <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>">Buyer</a>
                <input name="type" value="<?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'Buying' : 'Selling'?>" type="hidden" />
            </div>
            <div class="form-group">
                <input name="zipcode" placeholder="Enter the city or zip where you're buying or selling" value="" type="text" class="form-control address wpsl-search-form" autocomplete="off"/>
                <div class="error-msg text-danger"></div>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary submit form-control">Next <i class="fa fa-chevron-right" aria-hidden="true"></i> <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button>
            </div>
        </div>
        <div class="step-2 hidden">
            <h1 class="selling <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?>"><?php echo $settings['selling_step_2'] ?></h1>
            <h1 class="buying <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>"><?php echo $settings['buying_step_2'] ?></h1>
            <div class="form-group">
                <input type="hidden" name="kind" value=""/>
            </div>
            <div class="answers type">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-home"></i>
                            <p>Single Family</p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-building"></i>
                            <p>Condominium</p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-townhouse"></i>
                            <p>Townhouse</p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-mobilehome"></i>
                            <p>Mobile Home</p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-square-o"></i>
                            <p>Vacant Lot</p>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a class="submit">
                            <div class="">
                                <span class=""></span>
                            </div>
                            <i class="hi hi-office"></i>
                            <p>Commercial</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="step-3 hidden">
            <h1 class="selling <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?>"><?php echo $settings['selling_step_3'] ?></h1>
            <h1 class="buying <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>"><?php echo $settings['buying_step_3'] ?></h1>
            <div class="form-group">
                <input type="hidden" name="spend" value=""/>
            </div>
            <p><button type="button" class="btn btn-spend-value submit">$415k or less <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-spend-value submit">$415k - $550k <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-spend-value submit">$550k - $790k <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-spend-value submit">$790k - $1.6m <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-spend-value submit">$1.6m - $3.5m <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-spend-value submit">$3.5m or more <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
        </div>
        <div class="step-4 hidden">
            <h1 class="selling <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?>"><?php echo $settings['selling_step_4'] ?></h1>
            <h1 class="buying <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>"><?php echo $settings['buying_step_4'] ?></h1>
            <div class="form-group">
                <input type="hidden" name="timeline" value=""/>
            </div>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">ASAP <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">1-3 months <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">3-6 months <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">6-12 months <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">12+ months <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
        </div>
        <div class="step-5 hidden">
            <h1 class="selling <?php echo ((isset($_GET['page']) && $_GET['page'] == 'sell') || !isset($_GET['page'])) ? 'selected' : ''?>"><?php echo $settings['selling_step_5'] ?></h1>
            <h1 class="buying <?php echo (isset($_GET['page']) && $_GET['page'] == 'buy') ? 'selected' : ''?>"><?php echo $settings['buying_step_5'] ?></h1>
            <div class="form-group">
                <input type="hidden" name="property" value=""/>
            </div>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">Yes <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">No <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
        </div>
        <div class="step-6 hidden">
            <h1 class="selected"><?php echo $settings['first_step_6'] ?></h1>
            <h2 class="selected"><?php echo $settings['second_step_6'] ?></h2>
            <div class="form-group">
                <input name="name" placeholder="Full Name" value="" type="text" class="form-control"/>
                <div class="error-msg text-danger"></div>
            </div>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">Next <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
        </div>
        <div class="step-7 hidden">
            <h1 class="selected"><?php echo $settings['first_step_7'] ?></h1>
            <h2 class="selected"><?php echo $settings['second_step_7'] ?></h2>
            <div class="form-group">
                <input name="email" placeholder="Email" value="" type="text" class="form-control"/>
                <div class="error-msg text-danger"></div>
            </div>
            <div class="form-group">
                <input name="phone" placeholder="Phone" value="" type="text" class="form-control"/>
                <div class="error-msg text-danger"></div>
            </div>
            <p><button type="button" class="btn btn-lg btn-spend-value submit">Submit <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" class="loader-contact-form-plugin" style="display:none"/ ></button></p>
            <p class="terms"><small>By clicking "Submit", you electronically consent to our <a href="#">Terms of Service</a>, <a href="#">Privacy Policy</a> and to receive important communication electronically. You acknowledge that HomeLight will share your information with up to three of our real estate agent partners, and that consent is not a condition of purchase or receipt of services. You are providing express written consent for us or one of our partners to contact you via an auto-dialed telephone call, text message (SMS), and/or email, even if your number is currently listed on any internal, corporate, state, or federal Do-Not-Call list and even if you are charged for the call.
                </small></p>
        </div>
        <div class="step-8 hidden">
            <h1 class="selected"><?php echo $settings['step_8'] ?></h1>
            <p>Go back to the <a href="/">Home page</a> </p>
        </div>
    </form>
    <p class="result-msg"></p>
</div>
