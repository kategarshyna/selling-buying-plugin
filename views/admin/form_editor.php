<div class="container">
	<h1>Edit Contact Form</h1>
	<p class="description">
	<label for="shortcode">Copy this shortcode and paste it into your post, page, or text widget content:</label>
	<span><input type="text" id="shortcode" onfocus="this.select();" readonly="readonly" class="large-text code" value="[<?php echo esc_attr( $mail['short_code'] ); ?>]" /></span>
	</p>

	<div id="plugin_settings_contact_form">
		<form>
			<table class="form-table">
				<tbody>
                <tr>
                    <th scope="row">
                        <label for="mailChimpApiKey">MailChimp API</label>
                    </th>
                    <td>
                        <input name="mailChimpApiKey" value="<?php echo esc_html( $mail['mailChimpApiKey'] ) ?>" type="email" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="hubSpotApiKey">HubSpot API</label>
                    </th>
                    <td>
                        <input name="hubSpotApiKey" value="<?php echo esc_html( $mail['hubSpotApiKey'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="twilioApiKey">Twilio (SMS) Service API SID</label>
                    </th>
                    <td>
                        <input name="twilioApiKey" value="<?php echo esc_html( $mail['twilioApiKey'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="twilioToken">Twilio (SMS) Service API Auth Token</label>
                    </th>
                    <td>
                        <input name="twilioToken" value="<?php echo esc_html( $mail['twilioToken'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="twilioPhoneTo">Twilio (SMS) Service API Phone Number For SMS</label>
                    </th>
                    <td>
                        <input name="twilioPhoneTo" placeholder="Example:+380999999999" value="<?php echo esc_html( $mail['twilioPhoneTo'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="twilioBody">Twilio (SMS) Service API SMS Body Content</label>
                    </th>
                    <td>
                        <textarea rows="3" name="twilioBody" type="text" class="large-text code" size="70"><?php echo esc_html( $mail['twilioBody'] ) ?></textarea>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
				<tr>
					<th scope="row">
						<label for="to">To</label>
					</th>
					<td>
						<input name="to" value="<?php echo esc_html( $mail['to'] ) ?>" type="email" class="large-text code" size="70"/>
						<div class="error-msg text-danger"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="from">From</label>
					</th>
					<td>
						<input name="from" value="<?php echo esc_html( $mail['from'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg text-danger"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="sabject">Subject</label>
					</th>
					<td>
						<input name="subject" value="<?php echo esc_html( $mail['subject'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg text-danger"></div>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<label for="replay">Replay-To</label>
					</th>
					<td>
						<input name="replay" value="<?php echo esc_html( $mail['replay'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg text-danger"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="Message">Message</label>
					</th>
					<td>
						<textarea rows="7" name="message" type="text" class="large-text code" size="70"><?php echo esc_html( $mail['message'] ) ?></textarea>
						<div class="error-msg text-danger"></div>
					</td>
				</tr>
                <tr>
                    <th scope="row">
                        <label for="selling_step_1">Header Selling (step-1)</label>
                    </th>
                    <td>
                        <input name="selling_step_1" value="<?php echo esc_html( $mail['selling_step_1'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="buying_step_1">Header Buying (step-1)</label>
                    </th>
                    <td>
                        <input name="buying_step_1" value="<?php echo esc_html( $mail['buying_step_1'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="selling_step_2">Header Selling (step-2)</label>
                    </th>
                    <td>
                        <input name="selling_step_2" value="<?php echo esc_html( $mail['selling_step_2'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="buying_step_2">Header Buying (step-2)</label>
                    </th>
                    <td>
                        <input name="buying_step_2" value="<?php echo esc_html( $mail['buying_step_2'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="selling_step_3">Header Selling (step-3)</label>
                    </th>
                    <td>
                        <input name="selling_step_3" value="<?php echo esc_html( $mail['selling_step_3'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="buying_step_3">Header Buying (step-3)</label>
                    </th>
                    <td>
                        <input name="buying_step_3" value="<?php echo esc_html( $mail['buying_step_3'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="selling_step_4">Header Selling (step-4)</label>
                    </th>
                    <td>
                        <input name="selling_step_4" value="<?php echo esc_html( $mail['selling_step_4'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="buying_step_4">Header Buying (step-4)</label>
                    </th>
                    <td>
                        <input name="buying_step_4" value="<?php echo esc_html( $mail['buying_step_4'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="selling_step_5">Header Selling (step-5)</label>
                    </th>
                    <td>
                        <input name="selling_step_5" value="<?php echo esc_html( $mail['selling_step_5'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="buying_step_5">Header Buying (step-5)</label>
                    </th>
                    <td>
                        <input name="buying_step_5" value="<?php echo esc_html( $mail['buying_step_5'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="first_step_6">First Header (step-6)</label>
                    </th>
                    <td>
                        <input name="first_step_6" value="<?php echo esc_html( $mail['first_step_6'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="second_step_6">Second Header (step-6)</label>
                    </th>
                    <td>
                        <input name="second_step_6" value="<?php echo esc_html( $mail['second_step_6'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="first_step_7">First Header (step-7)</label>
                    </th>
                    <td>
                        <input name="first_step_7" value="<?php echo esc_html( $mail['first_step_7'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="second_step_7">Second Header (step-7)</label>
                    </th>
                    <td>
                        <input name="second_step_7" value="<?php echo esc_html( $mail['second_step_7'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="step_8">Header (step-8)</label>
                    </th>
                    <td>
                        <input name="step_8" value="<?php echo esc_html( $mail['step_8'] ) ?>" type="text" class="large-text code" size="70"/>
                        <div class="error-msg text-danger"></div>
                    </td>
                </tr>
				<tr>
					<td>
						<button type="button" class="button submit form-control">Save <img src="<?php echo \App\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" id="loader-contact-form-plugin" style="display:none"/ ></button>
					</td>
				</tr>
		</form>
		<p class="result-msg"></p>
	</div>
</div>
