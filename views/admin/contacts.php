<?php

use \App\Base\Config;

/**
 * @var ArrayObject[] $contacts
 */
?>
<h1>All Contacts</h1>
<table class="widefat fixed container" cellspacing="0">
	<thead>
	<tr>

		<th class="num" scope="col">ID</th>
		<th class="num" scope="col">Replay</th>
		<th class="num" scope="col">Date</th>
		<th class="num" scope="col">Subject</th>
		<th class="num" scope="col">Content</th>

	</tr>
	</thead>

	<tbody>
	<?php foreach ($contacts as $contact) {?>
		<?php $params = unserialize( base64_decode( $contact->post_content ) ); ?>
		<tr class="alternate">
			<th class="check-column num" scope="row">
				<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>&contact_id=<?php echo $contact->ID;?>">
					<?php echo $contact->ID;?>
				</a>
			</th>
			<td class="num">
				<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>&contact_id=<?php echo $contact->ID;?>">
					<?php echo esc_html( reset($params['replay'])) ?>
				</a>
			</td>
			<td class="num">
				<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>&contact_id=<?php echo $contact->ID;?>">
					<?php echo esc_html( $contact->post_date) ?>
				</a>
			</td>
			<td class="num">
				<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>&contact_id=<?php echo $contact->ID;?>">
					<?php echo esc_html( $params['subject']) ?>
				</a>
			</td>
			<td class="num">
				<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>&contact_id=<?php echo $contact->ID;?>">
					<?php echo strlen($params['message']) > 50 ?
						esc_html( substr( $params['message'], 0, 50) . '...') :
						esc_html( $params['message'] )?>
				</a>
			</td>

		</tr>
	<?php } ?>
	</tbody>
</table>

<?php if ( empty($contacts) ) echo '<br/><p class="container">Contact list is empty</p>' ?>

