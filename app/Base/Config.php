<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Base;

class Config
{
	public static $plugin_dir_path;
	public static $plugin_dir_url;
	public static $plugin;
	public static $plugin_settings_page;
	public static $plugin_contacts_page;
	public static $plugin_short_code;
	public static $plugin_option;

	public function initialize()
	{
		self::$plugin_dir_path = realpath(__DIR__ . '/../..' );
		self::$plugin_dir_url = plugin_dir_url( realpath(__DIR__ . '/../'));
		self::$plugin = realpath(__DIR__ . '/../../') . '/sell-buy-form-plugin.php';
		self::$plugin_settings_page = 'contact_form';
		self::$plugin_contacts_page = 'contacts';
		self::$plugin_short_code = 'sell-buy-form-plugin';
		self::$plugin_option = 'contact_form_settings';
	}
}
