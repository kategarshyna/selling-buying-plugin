<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Base;

class Enqueue
{
	public function initialize ()
	{
		add_action( 'admin_enqueue_scripts', array( $this, 'adminEnqueue' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'frontEnqueue' ) );
	}

	function frontEnqueue()
	{
		wp_enqueue_style( 'mypluginstyle', Config::$plugin_dir_url . 'assets/front.css?v=25');

		wp_register_script(
			'mypluginjs',
			Config::$plugin_dir_url . 'assets/front.js?v=23',
			array( 'jquery' )
		);
		wp_enqueue_script( 'mypluginjs' );

		wp_localize_script( 'mypluginjs', 'ajax_object',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' )
			)
		);
	}

	function adminEnqueue()
	{
		wp_enqueue_style( 'mypluginstyle', Config::$plugin_dir_url . 'assets/admin.css');

		wp_register_script(
			'mypluginjs',
			Config::$plugin_dir_url . 'assets/admin.js',
			array( 'jquery' )
		);
		wp_enqueue_script( 'mypluginjs' );

		wp_localize_script( 'mypluginjs', 'ajax_object',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' )
			)
		);
	}
}
