<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App;

final class Init
{
	public static function getServices()
	{
		return [
			get_class(new Base\Config),
			get_class(new Base\Enqueue),
			get_class(new Base\SettingsLinks),
			get_class(new Controllers\AdminController),
			get_class(new Controllers\FrontController),
		];
	}

	public static function initialize()
	{
		foreach ( self::getServices() as $class) {
			$service = self::instantiate( $class );
			if ( method_exists( $service, 'initialize' ) ) {
				$service->initialize();
			}
		}
	}

	private static function instantiate ( $class )
	{
		return new $class();
	}
}
