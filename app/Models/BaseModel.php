<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Models;

class BaseModel
{
	public $field = false;

	public $errors = array();

	public function getErrors() {
		return $this->errors;
	}

	public function rules()
	{
		return array();
	}

	public function load($data)
	{
		$this->setAttributes($data);
		return true;
	}

	public function setAttributes($values)
	{
		if (is_array($values)) {
			foreach ($values as $name => $value) {
				if (property_exists($this, $name)) {
					$this->$name = $value;
				}
			}
		}
	}

	public function validationRule($rule, $fieldName, $field, $value = null)
	{
		switch ($rule) {
			case 'email' :
				return preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $field) ?
					array() : array($fieldName => ucfirst($fieldName) . ' is not valid');
				break;
			case 'max' :
				return mb_strlen($field) <= $value ? array() : array(
					$fieldName => ucfirst($fieldName) . " must contain not more then $value characters."
				);
				break;
			case 'min' :
				return mb_strlen($field) >= $value ? array() : array(
					$fieldName => ucfirst($fieldName) . " must contain not less then $value characters."
				);
				break;
			case 'required' :
				return trim($field) ? array() :  array($fieldName => (ucfirst($fieldName) . " can not be blank."));
				break;
			case 'pattern' :
				return preg_match($value, $field) ?
					array() : array($fieldName => ucfirst($fieldName) . ' is not valid');
				break;
		}
	}

	public function validate()
	{
		if ($this->field ) {
			$validateResult = false;
		} else {
			$validateResult = true;
		}
		foreach ($this->rules() as $item) {
			list($keys, $rule) = $item;
			$ruleName = is_array($rule) ? key($rule) : $rule;
			$ruleValue = is_array($rule) ? current($rule) : null;
			if ( $this->field ) {
				if (property_exists($this, $this->field) && in_array($this->field, (array)$keys)) {
					$validateFieldResult = $this->validationRule(
						$ruleName,
						$this->field,
						$this->{$this->field},
						$ruleValue
					);
					if (!empty($validateFieldResult)) {
						$this->errors = array_merge( $this->errors, $validateFieldResult);
						return $validateResult;
					} else {
						$this->errors = array_merge( $this->errors, array($this->field => 'success'));
					}
				}
			} else {
				foreach ($keys as $key) {
					if (property_exists($this, $key) && !key_exists($key, $this->errors)) {
						$validateFieldResult = $this->validationRule($ruleName, $key, $this->$key, $ruleValue);
						if (!empty($validateFieldResult)) {
							$this->errors = array_merge( $this->errors, $validateFieldResult);
							$validateResult = false;
						}
					}
				}
			}
		}
		return $validateResult;
	}
}
