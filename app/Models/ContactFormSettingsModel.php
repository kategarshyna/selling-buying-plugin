<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Models;

use App\Base\Config;

class ContactFormSettingsModel extends BaseModel
{
	public $to;
	public $from;
	public $subject;
	public $replay;
	public $message;
	public $mailChimpApiKey;
	public $hubSpotApiKey;
	public $twilioApiKey;
	public $twilioToken;
	public $twilioPhoneTo;
	public $twilioBody;

	public $buying_step_1;
	public $selling_step_1;
	public $buying_step_2;
	public $selling_step_2;
	public $buying_step_3;
	public $selling_step_3;
	public $buying_step_4;
	public $selling_step_4;
	public $buying_step_5;
	public $selling_step_5;
	public $first_step_6;
	public $second_step_6;
	public $first_step_7;
	public $second_step_7;
	public $step_8;

	public function rules()
	{
		return array(
			array( array('to', 'from', 'subject', 'replay', 'message'), 'required'),
			array( array('message'), array('max' => 10000)),
			array( array('from', 'subject'), array('max' => 255)),
			array( array('subject', 'from'), array('min' => 2)),
			array( array('to'), 'email'),
		);
	}

	public static function getSettings()
	{
		$settingsArr = array(
			'to' => array(),
			'from' => array(),
			'subject' => array(),
			'message' => array(),
			'replay' => array(),
			'short_code' => Config::$plugin_short_code,
			'mailChimpApiKey' => array(),
			'hubSpotApiKey' => array(),
			'twilioApiKey' => array(),
			'twilioToken' => array(),
			'twilioPhoneTo' => array(),
			'twilioBody' => array(),
			'buying_step_1' => array(),
			'selling_step_1' => array(),
			'buying_step_2' => array(),
			'selling_step_2' => array(),
			'buying_step_3' => array(),
			'selling_step_3' => array(),
			'buying_step_4' => array(),
			'selling_step_4' => array(),
			'buying_step_5' => array(),
			'selling_step_5' => array(),
			'first_step_6' => array(),
			'second_step_6' => array(),
			'first_step_7' => array(),
			'second_step_7' => array(),
			'step_8' => array(),
		);

		if ( $settings = get_option( Config::$plugin_option) ) {
			$settings = unserialize( base64_decode( $settings ) );
			if ( ! empty($settings) ) {
				$settingsArr['to'] = isset ($settings['to']) ? $settings['to'] : '';
				$settingsArr['from'] = isset($settings['from']) ? $settings['from'] : '';
				$settingsArr['subject'] = isset($settings['subject']) ? $settings['subject'] : '';
				$settingsArr['message'] = isset($settings['message']) ? $settings['message'] : '';
				$settingsArr['replay'] = isset($settings['replay']) ? $settings['replay'] : '';
				$settingsArr['mailChimpApiKey'] = isset($settings['mailChimpApiKey']) ? $settings['mailChimpApiKey'] : '';
				$settingsArr['hubSpotApiKey'] = isset($settings['hubSpotApiKey']) ? $settings['hubSpotApiKey'] : '';
				$settingsArr['twilioApiKey'] = isset($settings['twilioApiKey']) ? $settings['twilioApiKey'] : '';
				$settingsArr['twilioToken'] = isset($settings['twilioToken']) ? $settings['twilioToken'] : '';
				$settingsArr['twilioPhoneTo'] = isset($settings['twilioPhoneTo']) ? $settings['twilioPhoneTo'] : '';
				$settingsArr['twilioBody'] = isset($settings['twilioBody']) ? $settings['twilioBody'] : '';
				$settingsArr['buying_step_1'] = isset($settings['buying_step_1']) ? $settings['buying_step_1'] : '';
				$settingsArr['selling_step_1'] = isset($settings['selling_step_1']) ? $settings['selling_step_1'] : '';
				$settingsArr['buying_step_2'] = isset($settings['buying_step_2']) ? $settings['buying_step_2'] : '';
				$settingsArr['selling_step_2'] = isset($settings['selling_step_2']) ? $settings['selling_step_2'] : '';
				$settingsArr['buying_step_3'] = isset($settings['buying_step_3']) ? $settings['buying_step_3'] : '';
				$settingsArr['selling_step_3'] = isset($settings['selling_step_3']) ? $settings['selling_step_3'] : '';
				$settingsArr['buying_step_4'] = isset($settings['buying_step_4']) ? $settings['buying_step_4'] : '';
				$settingsArr['selling_step_4'] = isset($settings['selling_step_4']) ? $settings['selling_step_4'] : '';
				$settingsArr['buying_step_5'] = isset($settings['buying_step_5']) ? $settings['buying_step_5'] : '';
				$settingsArr['selling_step_5'] = isset($settings['selling_step_5']) ? $settings['selling_step_5'] : '';
				$settingsArr['first_step_6'] = isset($settings['first_step_6']) ? $settings['first_step_6'] : '';
				$settingsArr['second_step_6'] = isset($settings['second_step_6']) ? $settings['second_step_6'] : '';
				$settingsArr['first_step_7'] = isset($settings['first_step_7']) ? $settings['first_step_7'] : '';
				$settingsArr['second_step_7'] = isset($settings['second_step_7']) ? $settings['second_step_7'] : '';
				$settingsArr['step_8'] = isset($settings['step_8']) ? $settings['step_8'] : '';
			}
		}
		return $settingsArr;
	}

	public static function install_default_settings()
	{
		if ( !get_option( Config::$plugin_option ) ) {
			$content = serialize([
				'to' => get_option('admin_email'),
				'from' => sprintf( '%s <%s>',
					get_bloginfo( 'name' ), self::from_email() ),
				'subject' => sprintf( '%1$s %2$s',
					get_bloginfo( 'name' ), '[your-subject]' ),
				'message' =>
					'Message Body:'
					. "\n" . '[your-message]' . "\n\n"
					. '-- ' . "\n"
					. sprintf( 'This e-mail was sent from a contact form on %1$s (%2$s)',
						get_bloginfo( 'name' ), get_bloginfo( 'url' ) ),
				'replay' => 'Reply-To: [your-name] <[your-email]>',
				'mailChimpApiKey' => '',
				'hubSpotApiKey' => '',
				'twilioApiKey' => '',
				'twilioToken' => '',
				'twilioPhoneTo' => '',
				'twilioBody' => 'New [your-type] Form filled! Phone:[your-phone] Name:[your-name]',
				'buying_step_1' => 'Buy a home or simply get a market value of your property',
				'selling_step_1' => 'Sell your home or simply get a market value of your property',
				'buying_step_2' => 'What kind of home are you looking for?',
				'selling_step_2' => 'What kind of home are you looking to sell?',
				'buying_step_3' => 'How much are you looking to spend?',
				'selling_step_3' => 'Roughly, how much is your home worth?',
				'buying_step_4' => 'How soon are you looking to buy?',
				'selling_step_4' => 'How soon are you looking to sell?',
				'buying_step_5' => 'Are you also looking to sell a property?',
				'selling_step_5' => 'Are you also looking to buy a property?',
				'first_step_6' => 'Now a little bit about you...',
				'second_step_6' => 'What is your name?',
				'first_step_7' => 'We can\'t wait to get in touch with you',
				'second_step_7' => 'we are almost there.',
				'step_8' => 'Thank you for reaching us! We will contact you soon.',

			]);
			return add_option( Config::$plugin_option, base64_encode( $content ), '');
		}
		return;
	}

	protected static function from_email() {
		$admin_email = get_option( 'admin_email' );
		$sitename = strtolower( $_SERVER['SERVER_NAME'] );

		if ( substr( $sitename, 0, 4 ) == 'www.' ) {
			$sitename = substr( $sitename, 4 );
		}

		if ( strpbrk( $admin_email, '@' ) == '@' . $sitename ) {
			return $admin_email;
		}

		return 'wordpress@' . $sitename;
	}


	public function save($validate = true)
	{
		if ($validate) {
			if ( !$this->validate()) {
				return false;
			}
		}

		$content = serialize([
			'to' => $this->to,
			'from' => $this->from,
			'subject' => $this->subject,
			'message' => $this->message,
			'replay' => $this->replay,
			'mailChimpApiKey' => $this->mailChimpApiKey,
			'hubSpotApiKey' => $this->hubSpotApiKey,
			'twilioApiKey' => $this->twilioApiKey,
			'twilioToken' => $this->twilioToken,
			'twilioPhoneTo' => $this->twilioPhoneTo,
			'twilioBody' => $this->twilioBody,
			'buying_step_1' => $this->buying_step_1,
			'selling_step_1' => $this->selling_step_1,
			'buying_step_2' => $this->buying_step_2,
			'selling_step_2' => $this->selling_step_2,
			'buying_step_3' => $this->buying_step_3,
			'selling_step_3' => $this->selling_step_3,
			'buying_step_4' => $this->buying_step_4,
			'selling_step_4' => $this->selling_step_4,
			'buying_step_5' => $this->buying_step_5,
			'selling_step_5' => $this->selling_step_5,
			'first_step_6' => $this->first_step_6,
			'second_step_6' => $this->second_step_6,
			'first_step_7' => $this->first_step_7,
			'second_step_7' => $this->second_step_7,
			'step_8' => $this->step_8,
		]);
		$content = base64_encode($content);

		if ( get_option( Config::$plugin_option ) ) {
			return update_option( Config::$plugin_option, $content );

		} else {
			return add_option( Config::$plugin_option, $content, '');
		}

		return false;
	}

}
