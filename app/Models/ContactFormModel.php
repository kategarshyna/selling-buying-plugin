<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Models;

use DrewM\MailChimp\MailChimp;
use Twilio\Rest\Client;

class ContactFormModel extends BaseModel
{
	public $name;
	public $zipcode;
	public $phone;
	public $type;
	public $kind;
	public $spend;
	public $property;
	public $timeline;
	public $email;
	public $subject;
	public $message;

	const post_type = 'pl_contact_form';
	const post_title = 'Contact';

	public function rules()
	{
		return array(
			array( array('name', 'email', 'zipcode', 'phone', 'type', 'kind', 'spend', 'property', 'timeline'), 'required'),
			//array(array('message'), array('max' => 10000)),
			array(array('name'), array('min' => 2) ),
			array(array('email'), 'email'),
			array(array('name'), array('pattern' => '/^[\p{L}\s\']+$/uis')),
			array(array('phone'), array('pattern' => '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/')),
		);
	}

	public function contact($email)
	{
		if ($this->validate()) {

			if ($this->type == 'Selling') {
				$this->message = 'Name: ' . $this->name . PHP_EOL .
				                 'Email: ' . $this->email . PHP_EOL .
				                 'Phone: ' . $this->phone . PHP_EOL .
				                 'Zipcode: ' . $this->zipcode . PHP_EOL .
				                 'Type: ' . $this->type . PHP_EOL .
				                 'What kind of home customer looking to sell: ' . $this->kind . PHP_EOL .
				                 'Roughly, how much is customers home worth: ' . $this->spend . PHP_EOL .
				                 'How soon is customers looking to sell: ' . $this->timeline . PHP_EOL .
				                 'Is customer also looking to buy a property? ' . $this->property . PHP_EOL;
			} else {
				$this->message = 'Name: ' . $this->name . PHP_EOL .
				                 'Email: ' . $this->email . PHP_EOL .
				                 'Phone: ' . $this->phone . PHP_EOL .
				                 'Zipcode: ' . $this->zipcode . PHP_EOL .
				                 'Type: ' . $this->type . PHP_EOL .
				                 'What kind of home customer looking for: ' . $this->kind . PHP_EOL .
				                 'How much is customer looking to spend: ' . $this->spend . PHP_EOL .
				                 'How soon is customer looking to buy: ' . $this->timeline . PHP_EOL .
				                 'IS customer also looking to sell a property: ' . $this->property . PHP_EOL;
			}

			$this->subject = 'New ' . $this->type . ' Form filled!';

			$settings = ContactFormSettingsModel::getSettings();
			$email = isset($settings['to']) ?  $settings['to'] : $email;
			$subject = isset($settings['subject']) ?
				str_replace('[your-subject]', $this->subject, $settings['subject'])
				: $this->subject;
			$message = isset($settings['message']) ?
				str_replace('[your-message]', $this->message, $settings['message'])
				: $this->message;
			$replay = isset($settings['replay']) ?
				str_replace('[your-name]', $this->name, $settings['replay'])
				: $this->name;
			$replay = isset($settings['replay']) ?
				str_replace('[your-email]', $this->email, $replay)
				: $this->email;

			$twilioBody = isset($settings['twilioBody']) ?
				str_replace('[your-name]', $this->name, $settings['twilioBody'])
				: "New $this->type Form filled! Phone:$this->phone Email:$this->email";
			$twilioBody = isset($settings['twilioBody']) ?
				str_replace('[your-phone]', $this->phone, $twilioBody)
				: "New $this->type Form filled! Phone:$this->phone Email:$this->email";
			$twilioBody = isset($settings['twilioBody']) ?
				str_replace('[your-type]', $this->type, $twilioBody)
				: "New $this->type Form filled! Phone:$this->phone Email:$this->email";


			if (!empty ($settings['mailChimpApiKey'])) {
				$this->mailchimpAddToList($settings['mailChimpApiKey']);
			}

			if (!empty ($settings['hubSpotApiKey'])) {
				$this->hubSpotAddContact($settings['hubSpotApiKey']);
			}

			if (!empty ($settings['twilioApiKey']) && !empty ($settings['twilioToken']) && !empty($settings['twilioPhoneTo'])) {
				$this->sendTwilioSMS($settings['twilioApiKey'],$settings['twilioToken'], $settings['twilioPhoneTo'], $twilioBody);
			}

			if (wp_mail( $email, $subject, $message, [$replay])) {
				return $this->saveContact();
			}
		}

		return false;
	}

	public function sendTwilioSMS ($sid, $token, $phoneTo, $twilioBody)
	{
		$client = new Client($sid, $token);
		$client->messages->create(
			$phoneTo,
			array(
				'from' => '+13479527045 ',
				'body' => $twilioBody
			)
		);
	}

	public function hubSpotAddContact ($hubspotKey)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://api.hubapi.com/contacts/v1/contact/?hapikey=$hubspotKey");
		curl_setopt($ch, CURLOPT_POST, 1);

		$contact = json_encode(
			array(
				'properties' => array(
					array(
						'property' => 'email',
						'value' => $this->email
					),
					array(
						'property' => 'firstname',
						'value' => $this->name
					),
					array(
						'property' => 'phone',
						'value' => $this->phone
					),
				)
			)
		);


		curl_setopt($ch, CURLOPT_POSTFIELDS, $contact);

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

//		if ($server_output != 'OK') {
//			var_dump($server_output); die;
//		}

	}

	public function mailchimpAddToList ($mailChimpApiKey)
	{
		$MailChimp = new MailChimp($mailChimpApiKey);
		$lists = $MailChimp->get('lists');

		$found = false;
		foreach ( $lists['lists'] as $key => $list ) {
			if ( $list['name'] == 'Sell Buy Form') {
				$found = true;
				$list_id = $list['id'];

				$result = $MailChimp->post("lists/$list_id/members", [
					'email_address' => $this->email,
					'status'        => 'subscribed',
				]);

//				if (!$MailChimp->success()) {
//					echo $MailChimp->getLastError();
//				}
				break;
			}
		}
		if (!$found) {
			$result = $MailChimp->post("/lists", [
				'name' => "Sell Buy Form",
				'contact' => [
					'company' => "Faruqi Team",
					'address1' => "675 Ponce De Leon Ave NE",
					'address2' => "",
					'city' => "",
					'state' => "",
					'zip' => "",
					'country' => "",
					'phone' => ""
				],
				'permission_reminder' => "You'\''re receiving this email because you subscribed for updates on Faruqi Team website.",
				'campaign_defaults' => [
					'from_name' => "Faruqi Team",
					'from_email' => "freddie@freddiehats.com",
					'subject' => "Faruqi Team",
					'language' => "en"
				],
				'email_type_option' => true
			]);
			if (isset($result['id']) && !empty($result['id'])) {
				$list_id = $result['id'];
				$result = $MailChimp->post("lists/$list_id/members", [
					'email_address' => $this->email,
					'status'        => 'subscribed',
				]);

//				if (!$MailChimp->success()) {
//					echo $MailChimp->getLastError();
//				}
			}
		}
	}

	public function register_post_type() {
		return register_post_type( self::post_type, array(
			'labels' => array(
				'name' => self::post_type,
				'singular_name' => self::post_type,
			),
			'rewrite' => false,
			'query_var' => false,
		) );
	}

	public function saveContact()
	{
		if ( !post_type_exists( self::post_type ) ) {
			$this->register_post_type();
		}

		if ( post_type_exists( self::post_type ) ) {
			$post_content = serialize([
				'subject' => $this->subject,
				'message' => $this->message,
				'replay' => [
					"Reply-To: $this->name <$this->email>"
				]
			]);
			if ( wp_insert_post( array(
				'post_type' => self::post_type,
				'post_status' => 'publish',
				'post_title' => self::post_title,
				'post_content' => base64_encode( $post_content ),
			), true )) {
				return true;
			}
		}
		return false;
	}

	public static function getContacts($id = null)
	{
		if ( !is_null($id) ) {
			return get_post( $id );
		}
		return get_posts( array( 'post_type' => self::post_type, 'numberposts' => -1 ) );
	}
}
