<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Controllers;

use App\Base\Config;
use App\Models\ContactFormModel;
use App\Models\ContactFormSettingsModel;

class FrontController
{
	public function initialize()
	{
		add_action( 'wp_ajax_front_contact_form_action', array($this, 'front_contact_form_action') );
		add_action( 'wp_ajax_nopriv_front_contact_form_action', array($this, 'front_contact_form_action') );
		add_shortcode( Config::$plugin_short_code, array( $this, 'run' ) );
	}

	public function run()
	{
		$settings = ContactFormSettingsModel::getSettings();
		require_once Config::$plugin_dir_path . "/views/front.php";
		return;
	}

	public function front_contact_form_action()
	{
		$contact_form = new ContactFormModel();
		if ($contact_form->load($_POST) && $contact_form->contact(get_option('admin_email'))){
			return wp_send_json([
				'status' => true,
				'messages' => 'Thank you! Your message send successfull.'
			]);
			wp_die();
		}

		return wp_send_json([
			'status' => false,
			'messages' => $contact_form->getErrors()
		]);

		wp_die();
	}

}
