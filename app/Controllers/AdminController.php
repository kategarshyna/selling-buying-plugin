<?php
/**
 * @package SellBuyFormPlugin
 */

namespace App\Controllers;

use App\Base\SettingsPages;
use App\Base\Config;
use App\Models\ContactFormModel;
use App\Models\ContactFormSettingsModel;

class AdminController
{
	public $settings;

	public function __construct()
	{
		$this->settings = new SettingsPages();
	}

	public function initialize ()
	{
		$pages = array(
			array(
				'page_title' => 'Sell-Buy Form',
				'menu_title' => 'Sell-Buy Form',
				'capability' => 'manage_options',
				'menu_slug' => 'contact_form',
				'callback' => array($this, 'admin_form_editor'),
				'icon_url' => 'dashicons-editor-table',
				'position' => 110
			),
			array(
				'page_title' => 'All Contacts',
				'menu_title' => 'All Contacts',
				'capability' => 'manage_options',
				'menu_slug' => 'contacts',
				'callback' => array($this, 'admin_all_contacts'),
			)
		);
		$this->settings->addPages( $pages )->initialize();
		ContactFormSettingsModel::install_default_settings();
		add_action( 'wp_ajax_save_contact_form_settings', array($this, 'save_contact_form_settings') );
	}


	public function admin_form_editor()
	{
		$mail = ContactFormSettingsModel::getSettings();
		require_once Config::$plugin_dir_path . "/views/admin/form_editor.php";
	}

	public function admin_all_contacts()
	{
		if ( isset($_GET['contact_id'] ) ) {
			$contact = ContactFormModel::getContacts( $_GET['contact_id'] );
			require_once Config::$plugin_dir_path . "/views/admin/contact.php";
			die;
		}
		$contacts = ContactFormModel::getContacts();
		require_once Config::$plugin_dir_path . "/views/admin/contacts.php";
	}

	public function save_contact_form_settings()
	{
		$contact_form_settings = new ContactFormSettingsModel();
		if ($contact_form_settings->load($_POST) && $contact_form_settings->save()){
			return wp_send_json([
				'status' => true,
				'messages' => 'Saved successfully.'
			]);
			wp_die();
		}

		return wp_send_json([
			'status' => false,
			'messages' => $contact_form_settings->getErrors()
		]);

		wp_die();
	}

}
